#!/bin/bash

# Set proxy
echo "Set proxy"
export http_proxy="http://proxy:3128"
export https_proxy="http://proxy:3128"
echo

git pull origin master

# Disable proxy
echo "Disable proxy"
unset http_proxy
unset https_proxy
echo "Done!"
