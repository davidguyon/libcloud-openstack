# LibCloud for OpenStack

This is my try to use LibCloud over OpenStack. I had some troubles for the first connection to the OpenStack API. I hope this work will help others. 

## OpenStack deployment

I deploy OpenStack on the Grid5000 platform and I use this deployment script: https://github.com/capi5k/capi5k-openstack

## testrc

The _testrc_ file contains the OpenStack user configuration. On my OpenStack deployment I also have _admin_ and _demo_. The _test_ user is the one I need because he is the tenant owner of the _test_ project and also an administrator. It means he is able to create instances and also access to the resource status. 

    source testrc
    nova keypair-add --pub_key /root/.ssh/id_rsa.pub jdoe_key

The second line is useful if you don't have access to the _jdoe_ keypairs. 
