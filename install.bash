#!/bin/bash
echo "Installation of needed tools"
echo
export http_proxy="http://proxy:3128"
export https_proxy="http://proxy:3128"
echo
echo "Setting up the virtualenv"
virtualenv env
source env/bin/activate
pip install apache-libcloud
echo
echo "Disable proxy"
unset http_proxy
unset https_proxy

