from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

import libcloud.security
import pprint
import os

pp = pprint.PrettyPrinter(indent=2)

# This assumes you don't have SSL set up.
# Note: Code like this poses a security risk (MITM attack) and
# that's the reason why you should never use it for anything else
# besides testing. You have been warned.
libcloud.security.VERIFY_SSL_CERT = False

USERNAME = os.getenv('OS_USERNAME')
PASSWORD = os.getenv('OS_PASSWORD')
AUTH_URL = os.getenv('OS_AUTH_URL') + "tokens/"
PROJECT_ID = os.getenv('OS_TENANT_NAME')
REGION = os.getenv('OS_REGION_NAME')

OpenStack = get_driver(Provider.OPENSTACK)
driver = OpenStack(USERNAME, PASSWORD,
                   ex_force_auth_url=AUTH_URL,
                   ex_tenant_name=PROJECT_ID,
                   ex_force_auth_version='2.0_password',
                   ex_force_service_type='compute',
                   ex_force_service_region=REGION,
                   ex_force_service_name='nova')

pp.pprint(driver.list_images())
pp.pprint(driver.list_sizes())
pp.pprint(driver.list_nodes())
pp.pprint(driver.ex_list_security_groups())
node = driver.create_node(name='test-node', 
                          image=driver.list_images()[0],
                          size=driver.list_sizes()[2],
                          ex_keyname='jdoe_key',
                          ex_security_groups=[driver.ex_list_security_groups()[1]])
